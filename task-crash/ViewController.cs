﻿using System;

using UIKit;
using System.Threading.Tasks;
using System.IO;
using Foundation;
using System.Threading;
using System.Collections.Generic;

namespace taskcrash
{
	public partial class ViewController : UIViewController
	{
		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.

			for (int i = 0; i < 10; ++i)
				Task.Run (() => ImageLoader ());
			Task.Run (() => Pressurizer ());
		}

		public class CachedBitmap
		{
			public int ReferenceCount { get; private set; }
			public UIImage Bitmap { get; private set; }

			static long _TotalCount;
			long _BitmapId;

			public CachedBitmap (UIImage bm)
			{
				Bitmap = bm;
				_BitmapId = _TotalCount++;
			}

			public void AddReference ()
			{
				ReferenceCount++;
				//Console.WriteLine ("ADD {0}", this);
			}

			public void ReleaseReference ()
			{
				--ReferenceCount;
				//Console.WriteLine ("RELEASE {0}", this);
				if (ReferenceCount == 0 && Bitmap != null) {
					//Bitmap.Recycle ();
					Bitmap.Dispose ();
					Bitmap = null;
				}
			}

			public override string ToString ()
			{
				return string.Format ("[CachedBitmap: Id={0}, ReferenceCount={1}]", _BitmapId, ReferenceCount);
			}
		}

		public async Task ImageLoader() {
			for(;;) {
				var img = await Task.Run (() => LoadImage());
				img.ReleaseReference ();
			}
		}

		//cause memory pressure by allocating some arrays
		public async Task Pressurizer() {
			var last = DateTime.Now;
			for(;;) {
				var o = new long[1024];
				var now = DateTime.Now;
				if (now - last > TimeSpan.FromMilliseconds (100)) {
					InvokeOnMainThread(() => {
						StatusText.Text = string.Format("GC count {0} {1} {2}", GC.CollectionCount(0), GC.CollectionCount(1), GC.GetTotalMemory(false));
					});
					last = now;
				}
			}
		}
		public async Task<CachedBitmap> LoadImage() {
			await Task.Yield ();
			var cb = new CachedBitmap (new UIImage ());
			cb.AddReference ();
			return cb;
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

